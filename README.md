# iTerminator

Python script to fake tmuxinator locally for iTerm2.

This script was built to mimic tmuxinator config file syntax but for local
iTerm2 tabs rather than tmux windows. One modification was added, you can set
a color for each window in the config file and iTerminator will set the tab
color.

## Get started

1. (Optional) Unzip and install the modified iTerm executable (iTerm+tabcolor.zip) in your
/Applications folder.
2. Install requirements `pip install -r requirements.txt`
3. Copy term to ~/bin (or somewhere in your path) and `chmod +x ~/bin/term`
4. Set up your iterminator script in ~/.tmuxinator
    mkdir ~/.tmuxinator; cp example.yml ~/.tmuxinator
5. Fire it up: `term example`